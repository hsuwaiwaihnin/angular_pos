<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        $data   = DB::SELECT("SELECT `id`,`name`,`image_path` FROM category");
        return $data;
    }
    public function fetchItem($id)
    {
        $data   = DB::SELECT("
        SELECT 
        T01.id,
        T01.name,
        T01.image_path,
        T01.price
        FROM  item T01
         WHERE T01.category_id = $id
        ");
        return $data;
    }
}
