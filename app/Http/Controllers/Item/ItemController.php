<?php

namespace App\Http\Controllers\Item;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ItemController extends Controller
{
    public function index($id)
    {
        $data   = DB::SELECT("SELECT `id`,`name`,`image_path`,`category_id`,`price` FROM item WHERE id = '$id' ");
        return $data;
    }
    public function search(Request $request)
    {
        $search = $request->get('search');
        $search = DB::table('item')
                    ->SELECT("id", "name", "image_path", "price")
                    ->WHERE('name', 'LIKE', $search.'%')
                    ->get();
        return $search;
    }
}
