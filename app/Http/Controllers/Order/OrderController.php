<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{
    public function index()
    {
        return view("order");
    }
    public function listing()
    {
        $data   = DB::SELECT("SELECT `id`,`total_price` FROM `order` ");
        return $data;
    }
    public function edit($id)
    {
        $data = [];
        $order_detail   = DB::table('order_detail')
                          ->SELECT(
                              "order_detail.id as order_detial_id",
                              "order_detail.item_id as id",
                              "order_detail.quantity",
                              "item.name",
                              "item.image_path",
                              "item.price"
                          )
                          ->LEFTJOIN("item", "item.id", "=", "order_detail.item_id")
                          ->WHERE("order_detail.order_id", "=", $id)
                          ->orderBy('order_detail.id', 'asc')
                          ->get();
        $total_price    = DB::table('order')
                          ->SELECT('total_price')
                          ->WHERE('id', '=', $id)
                          ->first();
        $data['order_detail']   = $order_detail;
        $data['total_price']    = $total_price->total_price;
        return $data;
    }
    public function delete($id)
    {
        $data   = DB::table('order')->where('id', $id)->delete();
        return $data;
    }
}
