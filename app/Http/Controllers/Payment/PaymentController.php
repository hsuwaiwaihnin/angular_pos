<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class PaymentController extends Controller
{
    public function index()
    {
        return view("payment");
    }
    public function edit($id)
    {
        $order = DB::table('order')->find($id);
        return view("payment")
                    ->with('order', $order);
    }
    public function charge(Request $request)
    {
        $response = [];
        DB::beginTransaction();
        try {
            $order_detail = $request->get('order_detail');
            $total  = $request->get('total');
            $max_id     = DB::table('order')->max('id');
            $order_id   = $max_id + 1;
            foreach ($order_detail as $key => $detail) {
                DB::table('order_detail')->insert(
                    [
                        'order_id' => $order_id,
                        'item_id' => $detail["id"],
                        'quantity' => $detail["quantity"]
                    ]
                );
            }
            DB::table('order')->insert(
                [
                    'id' => $order_id,
                    'total_price' => $total
                ]
            );
            DB::commit();
            $response['status'] = 200;
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response['status'] = 500;
            return response()->json($response);
        }
    }
    public function update(Request $request)
    {
        $response = [];
        DB::beginTransaction();
        try {
            $id           = $request->get('id');
            $order_detail = $request->get('order_detail');
            $total  = $request->get('total');
            $update = DB::table('order')
            ->where('id', $id)
            ->update(['total_price' => $total]);
            $delete = DB::table('order_detail')->where('order_id', $id)->delete();
            foreach ($order_detail as $key => $detail) {
                $insert = DB::table('order_detail')->insert(
                    [
                        'order_id' => $id,
                        'item_id' => $detail['id'],
                        'quantity' => $detail['quantity']
                    ]
                );
            }

            DB::commit();
            $response['status'] = 200;
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response['status'] = 500;
            return response()->json($response);
        }
    }

    public function searchItem($id)
    {
        $data   = DB::SELECT("SELECT `id`,`name`,`image_path`,`category_id`,`price` FROM item WHERE id = '$id' ");
        return $data;
    }
}
