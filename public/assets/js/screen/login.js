var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
  $scope.login    = function() {
    $http({
      method: "POST",
      url: "http://localhost:8000/api/login",
      data: {
        name:$scope.name,
        password:$scope.password,
      },
    }).then(
        function mySuccess(response) {
          window.localStorage.setItem('token', JSON.stringify(response.data));
          window.location.href = '/payment';
        },
        function myError(error) {
            alert("Error")
        }
    );
  }
});