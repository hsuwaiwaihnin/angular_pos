var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
    $scope.orders = []
    $scope.init     = function() {
        $http({
            method: "GET",
            url: "order/listing",
            data: {},
        }).then(
            function mySuccess(response) {
                $scope.orders = response.data;
                console.log($scope.orders)
            },
            function myError(error) {
                alert("Error")
            }
        );
    }
    $scope.edit  = function(id) {
        window.location.href = '/payment/edit/' + id;
    }
    $scope.delete = function (id) {
        $http({
            method: "GET",
            url: "order/delete/" + id,
            data: {},
        }).then(
            function mySuccess(response) {
                if(response.status == 200) {
                    $scope.init();
                }
            },
            function myError(error) {
                alert("Error")
            }
        );
    }
  
});