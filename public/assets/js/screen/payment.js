var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
  $scope.categoryId = undefined
  $scope.categoryName = undefined
  $scope.categories = []
  $scope.items  = [];
  $scope.showSearchItems = [];
  $scope.modal_id = undefined;
  $scope.modal_name = undefined;
  $scope.modal_image = undefined;
  $scope.modal_price = undefined;
  $scope.total = 0;
  $scope.order_details = []
  $scope.breadcrumb = undefined;
  $scope.category = true;
  $scope.item = false;
  $scope.showBreadCrumb = false;
  $scope.init     = function() {
    var order = $('#order').val();
    if(order != '') {
      $http({
        method: "GET",
        url: "/order/edit/" + order,
        data: {},
      }).then(
          function mySuccess(response) {
            var data = response.data
            $scope.order_details = data.order_detail;
            $scope.total  = data.total_price;
            $scope.getCategory()
          },
          function myError(error) {
              alert("Error")
          }
      );
    } else {
      $scope.getCategory() 
    }
  }
  $scope.getCategory = function() {
    $http({
      method: "GET",
      url: "/category",
      data: {},
    }).then(
        function mySuccess(response) {
          $scope.categories = response.data;
        },
        function myError(error) {
            alert("Error")
        }
    );
  }
  $scope.searching = function() {
    if($scope.search == '') {
      if(typeof $scope.categoryId !== "undefined" && typeof $scope.categoryName !== "undefined") {
        $scope.showItem($scope.categoryId,$scope.categoryName)
      }
      $scope.category = true;
      $scope.item = false;
    } else {
      $http({
        method: "POST",
        url: "/item/search",
        data: {
          search:$scope.search
        },
      }).then(
          function mySuccess(response) {
            $scope.items = response.data;
            $scope.category = false;
            $scope.item = true;
            $scope.showBreadCrumb = false;
          },
          function myError(error) {
              alert("Error")
          }
      );
    }
    
  }
  $scope.showItem   = function(id,name) {
    $scope.categoryId = id;
    $scope.categoryName = name;
    $http({
      method: "GET",
      url: "/category/" + id,
      data: {},
    }).then(
        function mySuccess(response) {
          if(response.status == 200) {
            $scope.items = response.data;
            $scope.showBreadCrumb = true;
            $scope.breadcrumb  = name;
            $scope.category = false;
            $scope.item = true;
          }
        },
        function myError(error) {
            alert("Error")
        }
    );
  }
 
  $scope.all = function(){
    $scope.categoryId = undefined;
    $scope.categoryName = undefined;
    $scope.category = true;
    $scope.item = false;
    $scope.showBreadCrumb = false;
  }

  $scope.addCart = function(id) {
    $http({
      method: "GET",
      url: "/item/" + id,
      data: {},
    }).then(
        function mySuccess(response) {
          if(response.status == 200) {
            var result = response.data[0];
            var process = false
            $scope.order_details = $scope.order_details.filter(element => {
              if(element.id == result.id) {
                process = true;
                element.quantity = element.quantity + 1
              }
              return element;
            });
            if(!process) {
              result.quantity = 1
              $scope.order_details.push(result)
            }
            $scope.calculatePayment();
            $('#exampleModal').modal('hide');
          }
        },
        function myError(error) {
            alert("Error")
        }
    );
  }

  $scope.increase = function(id) {
    $scope.order_details = $scope.order_details.filter(element => {
      if(element.id == id) {
        element.quantity = element.quantity + 1
      }
      return element;
    });
    $scope.calculatePayment();
  }
  $scope.decrease = function(id) {
    $scope.order_details = $scope.order_details.filter(element => {
      if(element.id == id) {
        if(element.quantity <= 1) {
          alert("You can not decrease this item");
        } else {
          element.quantity = element.quantity - 1
        }
      }
      return element;
    }); 
    $scope.calculatePayment();
  }

  $scope.delete = function(id) {
    $scope.order_details = $scope.order_details.filter(element => {
      return element.id != id;
    }); 
    $scope.calculatePayment();
  }

  $scope.cancel = function() {
    $scope.order_details = []
    $scope.total = 0
  }

  $scope.calculatePayment = function() {
    var total_amount = 0;
    for(var i= 0; i < $scope.order_details.length; i++) {
      var price = $scope.order_details[i].price;
      var quantity = $scope.order_details[i].quantity;
      var amount = price * quantity;
      total_amount = total_amount + amount;
    }
    $scope.total = total_amount
  }

  $scope.quickView = function(id) {
    $http({
      method: "GET",
      url: "/item/" + id,
      data: {},
    }).then(
        function mySuccess(response) {
          if(response.status == 200) {
            $scope.modal_id = response.data[0].id;
            $scope.modal_name = response.data[0].name;
            $scope.modal_image = response.data[0].image_path;
            $scope.modal_price = response.data[0].price;
            $('#exampleModal').modal('show');
          }
        },
        function myError(error) {
            alert("Error")
        }
    );
  }
  $scope.charge = function () {
    $http({
      method: "POST",
      url: "/payment/charge",
      data: {
        order_detail: $scope.order_details,
        total: $scope.total
    },
    }).then(
        function mySuccess(response) {
          if(response.status == 200) {
            alert("Success!");
            window.location.href = '/order';
          }
        },
        function myError(error) {
            alert("Error")
        }
    );
  }

  $scope.update = function() {
    var id = $('#order').val();
    $http({
      method: "POST",
      url: "/payment/update",
      data: {
        id: id,
        order_detail: $scope.order_details,
        total: $scope.total
    },
    }).then(
        function mySuccess(response) {
          if(response.status == 200) {
            alert("Success!");
            window.location.href = '/order';
          }
        },
        function myError(error) {
            alert("Error")
        }
    ); 
  }

  $scope.tokenTest   = function() {
    var headers = new Headers();
    var url     = "http://localhost:8000/api/students";
    headers.append('Content-Type', 'application/json');
    var getToken = JSON.parse(localStorage.getItem('token'));
    var token    = getToken.token;
    console.log(token)
    headers.append("Authorization", "Bearer " + token);

    $http({
      method: "POST",
      url: "http://localhost:8000/api/students",
      data: {},
      headers: { 
       'Content-Type': 'application/json' ,
       'Authorization': 'Bearer ' + token
     },
    }).then(
        function mySuccess(response) {
          if(response.status == 200) {
            console.log("hihi")
          }
        },
        function myError(error) {
            alert("Error")
        }
    ); 
  }
  
});