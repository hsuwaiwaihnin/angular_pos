<div class="col-md-8 card padding-y-sm card ">
    <ul class="nav bg radius nav-pills nav-fill mb-3 bg" role="tablist">
        <li class="nav-item">
            <a class="nav-link active show" data-toggle="pill" href="#nav-tab-card" ng-click="all()">
                <i class="fa fa-tags"></i> All</a>
        </li>
        <li class="nav-item" ng-if="showBreadCrumb">
            <i class="fa fa-tags "></i> @{{ breadcrumb }}
        </li>
    </ul>
    @include('category')
    @include('item')
</div>