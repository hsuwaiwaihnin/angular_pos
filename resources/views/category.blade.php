<span id="items" ng-if="category">
    <div class="row">
        <div class="col-md-3" ng-repeat="category in categories">
            <figure class="card card-product">
                <span class="badge-new"> NEW </span>
                <div class="img-wrap">
                    <img ng-src="/@{{category.image_path}}">
                </div>
                <figcaption class="info-wrap">
                    <a href="#" class="title" ng-click="showItem(category.id,category.name)">@{{ category.name }}</a>
                </figcaption>
            </figure> <!-- card // -->
        </div> <!-- col // -->
    </div> <!-- row.// -->
</span>