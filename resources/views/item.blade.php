<span id="items" ng-if="item">
    <div class="row">
        <div class="col-md-3" ng-repeat="item in items">
            <figure class="card card-product">
                <span class="badge-new"> NEW </span>
                <div class="img-wrap">
                    <img ng-src="/@{{item.image_path}}">
                    <a class="btn-overlay" href="#" ng-click="quickView(item.id)"><i class="fa fa-search-plus"></i>
                        Quick view</a>
                </div>
                <figcaption class="info-wrap">
                    <a href="#" class="title">@{{ item.name }}</a>
                    <div class="action-wrap">
                        <a href="#" class="btn btn-primary btn-sm float-right" ng-click="addCart(item.id)"> <i
                                class="fa fa-cart-plus"></i> Add
                        </a>
                        <div class="price-wrap h5">
                            <span class="price-new">$@{{ item.price }}</span>
                        </div> <!-- price-wrap.// -->
                    </div> <!-- action-wrap -->
                </figcaption>
            </figure> <!-- card // -->
        </div> <!-- col // -->
    </div> <!-- row.// -->
</span>