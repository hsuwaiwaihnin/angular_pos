<!DOCTYPE HTML>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Bootstrap-ecommerce by Vosidiy">
  <title>Angular Js Training - @yield('title')</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/images/logos/squanchy.jpg') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/images/logos/squanchy.jpg') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('assets/images/logos/squanchy.jpg') }}">
  <!-- jQuery -->
  <!-- Bootstrap4 files-->
  <link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('assets/css/ui.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('assets/fonts/fontawesome/css/fontawesome-all.min.css') }}" type="text/css"
    rel="stylesheet">
  <link href="{{ URL::asset('assets/css/OverlayScrollbars.css') }}" type="text/css" rel="stylesheet" />
  <link href="{{ URL::asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <script src="{{ URL::asset('assets/js/jquery-2.0.0.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('assets/js/OverlayScrollbars.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('assets/js/angular.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('assets/js/select2.min.js') }}" type="text/javascript"></script>

  <!-- Font awesome 5 -->
  <style>
    .avatar {
      vertical-align: middle;
      width: 35px;
      height: 35px;
      border-radius: 50%;
    }

    .bg-default,
    .btn-default {
      background-color: #f2f3f8;
    }

    .btn-error {
      color: #ef5f5f;
    }

    .product_view .modal-dialog {
      max-width: 800px;
      width: 100%;
    }

    .pre-cost {
      text-decoration: line-through;
      color: #a5a5a5;
    }

    .space-ten {
      padding: 10px 0;
    }

    .loader {
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      /* Safari */
      animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
      }
    }

    @keyframes spin {
      0% {
        transform: rotate(0deg);
      }

      100% {
        transform: rotate(360deg);
      }
    }
  </style>
  <!-- custom style -->
</head>

<body>