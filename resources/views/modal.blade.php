<div class="modal fade product_view" id="exampleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">@{{ modal_name }}</h3>
                <a href="#" data-dismiss="modal" class="class pull-right"><i class="fa fa-window-close"></i></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 product_img">
                        <img ng-src="@{{modal_image}}" class="img-responsive" style="width: 100%;">
                    </div>
                    <div class="col-md-6 product_content">
                        <h4>Product Id: <span>@{{ modal_id }}</span></h4>
                        <div class="rating">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            (10 reviews)
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy
                            text of the printing and typesetting industry.</p>
                        <h3 class="cost"><span class="glyphicon glyphicon-usd"></span> $@{{ modal_price }} </h3>
                        <div class="space-ten"></div>
                        <div class="btn-ground">
                            <button type="button" class="btn btn-primary" ng-click="addCart(modal_id)"><span
                                    class="glyphicon glyphicon-shopping-cart"></span> Add
                                To
                                Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>