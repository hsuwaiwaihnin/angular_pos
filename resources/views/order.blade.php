@extends('layouts.master')
@section('title', 'Order - SPA')
@section('content')
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y-sm bg-default ">
    <div class="container-fluid" ng-app="myApp" ng-init="init()" ng-controller="myCtrl">
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" class="th">Order ID</th>
                            <th scope="col" class="th">Total Price</th>
                            <th scope="col" class="th">Action</th>
                            {{-- <th scope="col" class="th">Delete</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="order in orders">
                            <th scope="row">@{{ order.id }}</th>
                            <td>$@{{ order.total_price }}</td>
                            <td>
                                <button type="button" class="m-btn btn btn-warning" ng-click="edit(order.id)">
                                    <i class=" fa fa-edit"></i></button>
                                {{--
                            </td>
                            <td> --}}
                                <button type="button" class="m-btn btn btn-danger" ng-click="delete(order.id)"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- container //  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
<script src="{{ URL::asset('assets/js/screen/order.js') }}" type="text/javascript"></script>
@endsection