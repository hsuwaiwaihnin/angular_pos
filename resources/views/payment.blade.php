@extends('layouts.master')
@section('title', 'Point of sale Payment System - SPA')
@section('content')
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y-sm bg-default " ng-app="myApp" ng-init="init()" ng-controller="myCtrl">
    @include('payment_header')
    <div class="container-fluid">
        <div class="row">
            @include('card')
            @include('table')
        </div>
        @include('modal')
    </div><!-- container //  -->
    <input type="hidden" value="{{(isset($order)) ? $order->id : ''}}" name="order" id="order" />
</section>

<!-- ========================= SECTION CONTENT END// ========================= -->
<script src="{{ URL::asset('assets/js/screen/payment.js?v=20220816') }}" type="text/javascript"></script>
@endsection