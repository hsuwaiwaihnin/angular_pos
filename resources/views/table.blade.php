<div class="col-md-4">
    <div class="card">
        <span id="cart">
            <table class="table table-hover shopping-cart-wrap">
                <thead class="text-muted">
                    <tr>
                        <th scope="col">Item</th>
                        <th scope="col" width="120">Qty</th>
                        <th scope="col" width="120">Price</th>
                        <th scope="col" class="text-right" width="200">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="detail in order_details">
                        <td>
                            <figure class="media">
                                <div class="img-wrap"><img ng-src="/@{{detail.image_path}}"
                                        class="img-thumbnail img-xs">
                                </div>
                                <figcaption class="media-body">
                                    <h6 class="title text-truncate"> @{{ detail.name }}</h6>
                                </figcaption>
                            </figure>
                        </td>
                        <td class="text-center">
                            <div class="m-btn-group m-btn-group--pill btn-group mr-2" role="group" aria-label="...">
                                <button type="button" class="m-btn btn btn-default" ng-click="decrease(detail.id)"><i
                                        class="fa fa-minus"></i></button>
                                <button type="button" class="m-btn btn btn-default" disabled>@{{ detail.quantity
                                    }}</button>
                                <button type="button" class="m-btn btn btn-default" ng-click="increase(detail.id)"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                        </td>
                        <td>
                            <div class="price-wrap">
                                <var class="price">$@{{ detail.price }}</var>
                            </div> <!-- price-wrap .// -->
                        </td>
                        <td class="text-right">
                            <a href="" class="btn btn-outline-danger" ng-click="delete(detail.id)"> <i
                                    class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </span>
    </div> <!-- card.// -->

    <div class="box">
        <dl class="dlist-align">
            <dt>Total: </dt>
            <dd class="text-right h4 b"> $@{{total}} </dd>
        </dl>
        <div class="row">
            <div class="col-md-6">
                <button class="btn  btn-default btn-error btn-lg btn-block" ng-click="cancel()" {{ isset($order)
                    ? 'disabled' : '' }}><i class="fa fa-times-circle "></i>
                    Cancel </button>
            </div>
            <div class="col-md-6">
                @if(isset($order))
                <a href="#" class="btn  btn-primary btn-lg btn-block" ng-click="update()"><i
                        class="fa fa-shopping-bag"></i> Update </a>
                @else
                <a href="#" class="btn  btn-primary btn-lg btn-block" ng-click="charge()"><i
                        class="fa fa-shopping-bag"></i> Charge </a>
                @endif
            </div>
        </div>
    </div> <!-- box.// -->
</div>