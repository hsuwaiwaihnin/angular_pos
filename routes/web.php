<?php

use App\Http\Controllers\Payment\PaymentController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Item\ItemController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Login\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login',[LoginController::class, 'index']);
Route::group(['namespace' => 'payment','prefix' => 'payment'], function () {
    Route::get('/', [PaymentController::class, 'index']);
    Route::get('/edit/{id}', [PaymentController::class, 'edit']);
    Route::POST('/charge', [PaymentController::class, 'charge']);
    Route::POST('/update', [PaymentController::class, 'update']);
});

Route::group(['namespace' => 'category','prefix' => 'category'], function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/{id}', [CategoryController::class, 'fetchItem']);
});

Route::group(['namespace' => 'item','prefix' => 'item'], function () {
    Route::get('/{id}', [ItemController::class, 'index']);
    Route::POST('/search', [ItemController::class, 'search']);
});

Route::group(['namespace' => 'order','prefix' => 'order'], function () {
    Route::get('/', [OrderController::class, 'index']);
    Route::get('/listing', [OrderController::class, 'listing']);
    Route::get('/edit/{id}', [OrderController::class, 'edit']);
    Route::get('/delete/{id}', [OrderController::class, 'delete']);
});
